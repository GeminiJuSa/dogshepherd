package com.gemtastic.doggydaycare;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 *
 * @author Aizic Moisen
 */
public class LogInController {
    
    @FXML
    private TextField username;
    
    @FXML
    private PasswordField password;
    
    @FXML
    private Button attemptLogIn;
    
    
    @FXML
    private void logInAttempt(){
        Navigator.loadScreen(Navigator.MAIN_SCREEN);
    }
}
