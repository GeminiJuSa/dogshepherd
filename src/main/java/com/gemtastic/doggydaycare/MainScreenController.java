package com.gemtastic.doggydaycare;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

/**
 *
 * @author Aizic Moisen
 */
public class MainScreenController {
    
    private String informationMessage;
    
    @FXML
    private Label information;
    
    @FXML
    private Button viewDogs;
    
    @FXML
    private Button registerDog;
            
    @FXML
    private Button viewServices;
    
    
    @FXML
    private void goToServices(){
        Navigator.loadScreen(Navigator.SERVICES);
    }
}
