package com.gemtastic.doggydaycare;

import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;

/**
 *
 * @author Aizic Moisen
 */
public class Navigator {
    public static final String LOGIN = "/com/gemtastic/fxml/LogInScreen.fxml";
    public static final String MAIN_SCREEN = "/com/gemtastic/fxml/MainScreen.fxml";
    public static final String SERVICES = "/com/gemtastic/fxml/Services.fxml";
    
    private static MainFrameController mainController;
    
    public static void setMainController(MainFrameController mainController){
        Navigator.mainController = mainController;
    }
    
    public static void loadScreen(String fxml){
        try{
            mainController.setScreen((Node)FXMLLoader.load(Navigator.class.getResource(fxml)));
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
