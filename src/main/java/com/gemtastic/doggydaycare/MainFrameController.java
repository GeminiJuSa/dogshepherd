package com.gemtastic.doggydaycare;

import java.awt.Desktop;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

/**
 *
 * @author Aizic Moisen
 */
public class MainFrameController implements Initializable{
    
    private final HostServices host;

    private DropShadow exitAppGlow;
    private Tooltip exitTooltip;
    
    public MainFrameController(HostServices host){
        this.host = host;
    }
    
    @FXML
    private ImageView exitApp;
    
    @FXML
    private Hyperlink termsOfService; 
    
    @FXML
    private Hyperlink contact;
    
    @FXML
    private BorderPane display;
    
    
    @FXML
    private void logInAttempt(){
//        System.out.println(username.getText());
//        System.out.println(password.getText());
        
//        display.getChildren().removeAll();
////        display.getChildren().setAll(FXMLLoader.load("MainScreen.fxml"));
    }
    
    @FXML
    private void termsClicked(){
        System.out.println("Adorableness on the way!");
        host.showDocument("http://youtu.be/SyyBx6-VMQg");
    }
    
    /**
     * The method for when you click the "contact us" hyperlink.
     * @throws IOException 
     */
    @FXML
    private void contactClicked() throws IOException{
        System.out.println("You clicked the contact link!");
        
        
        final InputStream in = Start.class.getResourceAsStream("/com/gemtastic/html/contact.html");
        if(in == null){
            throw new IOException("contact.html not found in resources");
        }
        final Path tempFile = Paths.get(System.getProperty("java.io.tmpdir"), "contact.html");
        Files.deleteIfExists(tempFile);
        try(final InputStream in2 = in;){
            Files.copy(in2, tempFile);
        }
        Desktop.getDesktop().browse(tempFile.toRealPath().toUri());
    }
    
    /**
     * This is the method for when you click the {@code exitApp}-button ({@code ImageView}).<p>
     * 
     * It is a little bit poorly named since this was my very first attempt at 
     * using the code section of scene builder and I don't know how I thought
     * this stuff works and whenever I try to refactor or rename anything my 
     * NetBeans goes to the 7th circuit of hell to bring back HitlerRapesYouException.<p>
     * 
     * Nothing to do here *flies away*
     */
    @FXML
    private void onClicked(){
        System.out.println("Bye bye!");
        System.exit(0);
    }
    /**
     * Also an event for the {@code exitApp}-button ({@code ImageView}), this one for
     * hovering the image.<p>
     * 
     * Will create a red glow{@code DropShadow} on the image as well as an
     * informative {@code Tootip}.
     */
    @FXML
    private void mouseEnter(){
        exitAppGlow = new DropShadow(9.0, Color.RED);
        exitTooltip = new Tooltip("Exit application");
        Tooltip.install(exitApp, exitTooltip);
        exitApp.setEffect(exitAppGlow);
    }
    /**
     * The event for when the mouse leaves the {@code exitApp}-button {@code ImageView}.
     */
    @FXML
    private void mouseExit(){
        exitAppGlow.setRadius(0.0);
    }
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TODO
    }
    
    public void setScreen(Node node) {
        display.setCenter(node);
    }
    
}
