package com.gemtastic.doggydaycare;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 *
 * @author Aizic Moisen
 */
public class Start extends Application{
    public static void main(String... ignored){
        launch(ignored);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        FXMLLoader loader = new FXMLLoader();
        
        loader.setController(new MainFrameController(getHostServices()));
        loader.setLocation(Start.class.getResource("/com/gemtastic/fxml/MainView.fxml"));
        
        AnchorPane root = loader.load();
        
        MainFrameController mainFrameController = loader.getController();
        Navigator.setMainController(mainFrameController);
        Navigator.loadScreen(Navigator.LOGIN);
        
        Scene scene = new Scene(root);
        scene.setFill(null);
        
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.TRANSPARENT);
        primaryStage.show();
    }
}
